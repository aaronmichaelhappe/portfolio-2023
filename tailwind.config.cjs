/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: "jit",
  content: ["./src/**/*.{html,js,svelte,ts}"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Futura", "sans"],
      },
      colors: {
        myBlack: "#010102",
        myYellow: {
          DEFAULT: "#F59E0B",
          50: "#FCE4BB",
          100: "#FBDCA8",
          200: "#FACD81",
          300: "#F8BD59",
          400: "#F7AE32",
          500: "#F59E0B",
          600: "#C07C08",
          700: "#8A5906",
          800: "#543603",
          900: "#1E1401",
        },
        myGreenDk: "#3fc45f",
        myGreen: {
          DEFAULT: "#2DD259",
          50: "#C4F2D1",
          100: "#B3EFC3",
          200: "#92E8A9",
          300: "#70E08E",
          400: "#4FD974",
          500: "#2DD259",
          600: "#23A445",
          700: "#197632",
          800: "#0F471E",
          900: "#05190B",
        },
        myBlue: {
          DEFAULT: "#4A56EC",
          50: "#F0F1FD",
          100: "#DEE0FC",
          200: "#B9BDF8",
          300: "#949BF4",
          400: "#6F78F0",
          500: "#4A56EC",
          600: "#1827E6",
          700: "#131FB3",
          800: "#0D1680",
          900: "#080D4D",
        },
        myRed: {
          DEFAULT: "#D21A00",
          50: "#FF998B",
          100: "#FF8776",
          200: "#FF634D",
          300: "#FF4025",
          400: "#FB1F00",
          500: "#D21A00",
          600: "#9A1300",
          700: "#620C00",
          800: "#2A0500",
          900: "#000000",
        },
        myGray: {
          DEFAULT: "#78787F",
          50: "#f2f2f3",
          100: "#E5E5E6",
          200: "#C8C8CB",
          300: "#AEAEB2",
          400: "#949499",
          500: "#78787F",
          600: "#616166",
          700: "#48484C",
          800: "#2F2F32",
          900: "#19191A",
        },
      },
    },
  },
};
