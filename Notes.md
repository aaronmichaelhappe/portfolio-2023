# Portolio Notes

## TODO:

- comb over code before deploy, check performance in dev tools
- SVGs - look into further optimization (maybe)
- (portfolio/+page.svelte) -> double check anime.js and proper way to do concurrent animations, is it several objects? or one like a timeline but concurrent
- consider breaking down hp into more components -- logo and nav
- custimze button component more to be more flexible
- convert bg stripes into tailwind
- look for other taiwind converstion situations and applys/breaking into components

## Tailwind related

---

->> px-2 py-3 apply-layout-ph <<--This is my way of prepping for apply before actually prematurely adding an apply. I put these classes first (or second if after rare named classes).

The remaining tailwind utils are ordered close ish to the recommend ordering convetion.

content -> position (including top, z-index etc) -> display -> sizing -> margin/padding -> other (overflow, etc) -> text -> alpha remaining decorative props

IE: relative top-0 z-10 block w-0 h-0 p-2 bg-red border-white

Styling situations like 'drawing' with css, are some of the few situations I am not fond of using tailwind. I try to keep non-tailwind css light and isolated.

I also like using apply when I toggle classes with css

## keep track of...

---

z-30 Header.svelte
z-30 src/routes/+layout.svelte
z-20 Header.svelte
z-10 src/routes/+layout.svelte
z-0 BgPattern.svelte
z-[-1] src/routes/+page.svelte

## individual pages
