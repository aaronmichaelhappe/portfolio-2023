// sc: imp-kit
import { json } from "@sveltejs/kit";
import type { RequestHandler } from "./$types";
import db from "$lib/database";

// sc: get-req-func
export const GET: RequestHandler = async () => {
  // const posts = [
  //   {
  //     slug: "sveltekit",
  //     content: `<h1>SvelteKit</h1><p>asdf asdf asdf</p>`,
  //   },
  // ];
  // return json(posts);

  const posts = await db.post.findMany();

  return json(posts);
};
