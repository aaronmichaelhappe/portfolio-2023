import type { RequestHandler } from "./$types";

export const GET: RequestHandler = async (event) => {
  const options: ResponseInit = {
    status: 418,
    headers: {
      X: "here is a header",
    },
  };
  return new Response("Hey yo, hey.", options);
};

export const POST: RequestHandler = async (event) => {
  const data = await event.request.formData();
  const email = await data.get("email");

  console.log(email);

  return new Response(
    JSON.stringify({
      success: true,
    }),
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
};
