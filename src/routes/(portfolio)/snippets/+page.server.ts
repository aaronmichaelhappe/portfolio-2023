import type { PageServerLoad } from "./$types";

export const load: PageServerLoad = () => {
  const post = {
    slug: "sveltekit",
    content: `<h1>SvelteKit</h1><p>example</p>`,
  };

  return { post };
};
